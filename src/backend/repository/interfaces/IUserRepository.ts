import { IRepository } from "./IRepository";
import { User } from "@backend/models";
//@ts-ignore
export interface IUserRepository extends IRepository<typeof User> {}

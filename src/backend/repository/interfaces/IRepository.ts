import { Model } from "objection";
export interface IRepository<T extends typeof Model> {
  create(data: any): Promise<T["prototype"]>;
  updateById(id: number | string, data: any): Promise<T["prototype"]>;
  findById(id: number | string): Promise<T["prototype"] | undefined>;
  deleteById(id: number | string): Promise<boolean>;
  insertBatch(data: any[]): Promise<T["prototype"][]>;
  insertBatchOnConflict(
    data: any[],
    columns: string[]
  ): Promise<T["prototype"][]>;
}

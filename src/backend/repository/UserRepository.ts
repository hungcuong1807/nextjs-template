import { Repository } from "./Repository";
import { User } from "@backend/models";
import { IUserRepository } from "@backend/repository/interfaces";

import { injectable } from "inversify";

@injectable()
//@ts-ignore
export class UserRepository
  extends Repository<typeof User>
  implements IUserRepository
{
  initializeModel(): typeof User {
    return User;
  }
}

import { IRepository } from "@backend/repository/interfaces";
import { injectable } from "inversify";
import { Model } from "objection";

@injectable()
export abstract class Repository<T extends typeof Model>
  implements IRepository<T>
{
  private _model!: T;

  constructor() {
    this.makeModel();
  }

  get model(): T {
    return this._model;
  }

  abstract initializeModel(): T;

  static queryFilter(query: any, filter: any): any {
    return query;
  }

  public makeModel() {
    this._model = this.initializeModel();
    if (!this._model) {
      throw new Error("Not found model. Please set model by setModel method");
    }
  }

  async create(data: any): Promise<T["prototype"]> {
    const result = await this.model.query().insert(data);
    return result;
  }

  async deleteById(id: number | string): Promise<boolean> {
    const number = await this.model.query().deleteById(id);
    return number > 0;
  }

  async findById(id: number | string): Promise<T["prototype"] | undefined> {
    const result = await this.model.query().findById(id);
    return result;
  }

  async updateById(id: number | string, data: any): Promise<T["prototype"]> {
    const result = await this.model.query().updateAndFetchById(id, data);
    return result;
  }

  async insertBatch(data: any[]): Promise<T["prototype"][]> {
    const result = await this.model.query().insert(data);
    return result as unknown as T["prototype"][];
  }

  async insertBatchOnConflict(
    data: any[],
    columns: string[]
  ): Promise<T["prototype"][]> {
    const result = await this.model
      .query()
      .insert(data)
      .onConflict(columns)
      .merge();
    return result as unknown as T["prototype"][];
  }
}

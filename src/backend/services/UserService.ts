import { IUserService } from "@backend/services/interfaces";
import { IUserRepository } from "@backend/repository/interfaces";
import { inject, injectable } from "inversify";
import { REPOSITORIES } from "@constants/injections";
import { SUser } from "./types";

@injectable()
export class UserService implements IUserService {
  @inject(REPOSITORIES.UserRepository) private userRepository!: IUserRepository;
  async findById(id: string | number): Promise<SUser | undefined> {
    try {
      const user = await this.userRepository.findById(id);
      return user as SUser;
    } catch (error: any) {
      console.log(error);
    }
  }
}

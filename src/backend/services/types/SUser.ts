export type SUser = {
  id: string;
  email: string;
  role: string | null;
};

import { SUser } from "@backend/services/types";
export interface IUserService {
  findById(id: number | string): Promise<SUser | undefined>;
}

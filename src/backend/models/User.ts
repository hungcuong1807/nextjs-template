import { db } from "@configs/database";
import { v4 as uuidv4 } from "uuid";
import * as bcrypt from "bcrypt";

import BaseModel from "./BaseModel";

class User extends BaseModel {
  id!: string;
  email!: string;
  password: string | undefined | null;
  role: string | undefined | null;

  static tableName: string = "users";
  static jsonSchema = {
    type: "object",

    properties: {
      id: { type: "string" },
      email: { type: "string" },
      password: { type: ["string", "null"], default: null },
      role: { type: ["string", "null"], default: null },
    },
  };

  async beforeInsert() {
    this.id = uuidv4();
    if (this.password) {
      this.password = await bcrypt.hash(
        this.password,
        Number(process.env.PASSWORD_SALT_ROUND) || 10
      );
    }
  }
}
const UserModel = User.bindKnex(db);
export default UserModel;

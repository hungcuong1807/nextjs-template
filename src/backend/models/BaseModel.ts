import { softDelete } from "@configs/database";
import { Model, AjvValidator } from "objection";
//@ts-ignore
class BaseModel extends softDelete(Model) {
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;

  // static jsonSchema = {
  //     properties: {
  //         created_at: { type: "string" },
  //         updated_at: { type: "string" },
  //     }
  // }

  beforeInsert() {}

  static createValidator() {
    return new AjvValidator({
      onCreateAjv: (ajv) => {
        // Here you can modify the `Ajv` instance.
      },
      options: {
        allErrors: true,
        validateSchema: false,
        ownProperties: true,
      },
    });
  }
  $beforeValidate(jsonSchema: any, json: any, opt: any) {
    return jsonSchema;
  }

    $beforeInsert() {
    this.created_at = new Date().toISOString();
    this.updated_at = new Date().toISOString();
    this.beforeInsert();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

export default BaseModel;

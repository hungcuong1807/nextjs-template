// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { IUserService } from "@backend/services/interfaces";
import { SUser } from "@backend/services/types";
import container, { lazyInject } from "@configs/container";
import { SERVICES } from "@constants/injections";
import type { NextApiRequest, NextApiResponse } from "next";
import "reflect-metadata";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<SUser | undefined>
) {
  const userService: IUserService = container.get<IUserService>(
    SERVICES.UserService
  );
  const user = await userService.findById(
    "ea823a26-7cae-4bfa-a4e6-dbbebf6a3390"
  );

  res.status(200).json(user);
}

export const REPOSITORIES = {
  UserRepository: Symbol.for("UserRepository"),
};

export const SERVICES = {
  UserService: Symbol.for("UserService"),
};

import { IUserRepository } from "@backend/repository/interfaces";

import { UserRepository } from "@backend/repository";
import { Container } from "inversify";
import getDecorators from "inversify-inject-decorators";
import { REPOSITORIES, SERVICES } from "@constants/injections";
import { IUserService } from "@backend/services/interfaces";
import { UserService } from "@backend/services";

const container = new Container({ defaultScope: "Singleton" });

// Binding Repositories
container.bind<IUserRepository>(REPOSITORIES.UserRepository).to(UserRepository);

// Binding Service
container.bind<IUserService>(SERVICES.UserService).to(UserService);
const { lazyInject } = getDecorators(container);
export { lazyInject };
export default container;

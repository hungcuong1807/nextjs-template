import Knex, { Knex as K } from "knex";
import objectionSoftDelete from "objection-js-soft-delete";
import KnexLogger from "./knexLogging";

const config: { [key: string]: K.Config } = {
  production: {
    client: "postgresql",
    connection: {
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      database: process.env.DB_DATABASE,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "migrations",
      extension: "ts",
      directory: "../backend/database/migrations/production",
    },
    seeds: {
      extension: "ts",
      directory: ".../backend/database/seeders/production",
    },
  },
};

const db =
  process.env.APP_ENV !== "production"
    ? KnexLogger(Knex(config.production))
    : Knex(config.production);

const softDelete = objectionSoftDelete({
  columnName: "deleted_at",
  deletedValue: new Date(),
  notDeletedValue: null,
});

export { db, softDelete };
export default config
